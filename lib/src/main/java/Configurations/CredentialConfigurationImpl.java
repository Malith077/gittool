package Configurations;

import java.nio.file.Path;
import java.util.Optional;
import static Configurations.TextFormatter.*;

import com.fasterxml.jackson.annotation.JsonProperty;

// We will come to this implementation later on, for now, Let's focus on SSH factory

public final class CredentialConfigurationImpl implements CredentialConfiguration{
	
	@JsonProperty("username")
	private final String username;
	
	@JsonProperty("password")
	private final String password;
	
	@JsonProperty("sshKeyPath")
	private final String sshKeyPath;

	
	public CredentialConfigurationImpl() {
		this.username = "";
		this.password = "";
		this.sshKeyPath = "";
	}
	
	@Override
	public Optional<String> password() {
		return optStr(password);
	}

	@Override
	public Optional<String> username() {
		return Optional.empty();
	}

	@Override
	public Optional<Path> sshKeyPath() {
		return Optional.empty();
	}
	
	
	
}
