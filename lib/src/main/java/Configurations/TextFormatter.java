package Configurations;

import java.nio.file.Path;
import java.util.Optional;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

public final class TextFormatter {

	private static Pattern REGEX_PATTERNS = Pattern.compile("");
	
	public TextFormatter() {
		
	}
	
	public static String formatPath(final Path path) {
		return formatPath(path.toString());
	}
	
	public static String formatPath(final String path) {
		return path.replace("\\", "/");
	}
	
	public static String formatText(final String str) {
		return StringUtils.replace(StringUtils.replace(lenientFormat(str), "[", "("), "]", ")");
	}
	
	public static String lenientFormat(final String str) {
		return StringUtils.replace(StringUtils.replace(StringUtils.replace(str, "\r\n", "\n"), "&nbsp;", " "), "\t", "	");
	}
	
	public static Optional<String> optStr(final String string){
		return Optional.ofNullable(string).filter(x -> !x.isEmpty()).map(TextFormatter::formatText);
	}
}
