package Configurations;

import java.nio.file.Path;
import java.util.Optional;

public interface CredentialConfiguration {

	Optional<String> password();
	Optional<String> username();
	
	Optional<Path> sshKeyPath();
}
