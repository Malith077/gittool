package GitProject;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.TimeZone;
import java.util.stream.Collectors;

import javax.swing.Spring;

import org.apache.commons.io.FileUtils;
import org.eclipse.jgit.api.CheckoutCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.ListBranchCommand.ListMode;
import org.eclipse.jgit.api.PushCommand;
import org.eclipse.jgit.api.Status;
import org.eclipse.jgit.api.TransportConfigCallback;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.NoFilepatternException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.lib.Config;
import org.eclipse.jgit.lib.PersonIdent;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.transport.SshTransport;



public class GitService {
	
	private final String repository; //Repository to be cloned
	private final Path cloneDirectory; // Path in the file system 
	private final Git git;
	private final String userName;
	private final String userEmail;
	
	private final TransportConfigCallback transport;
	
	private static final String USER_HOME = "user.home";
	
	public GitService(final String aRepository, final String aCloneDirectory, final String aUserName, final String aUserEmail) {
		this.repository = aRepository;
		this.cloneDirectory = Paths.get(aCloneDirectory).toAbsolutePath().normalize();
		this.userName = aUserName;
		this.userEmail = aUserEmail;
		try {
			git = Git.init().setDirectory(this.cloneDirectory.toFile()).call();
		}  catch (GitAPIException e) {
			throw new IllegalStateException("Could not instantiate Git instance");
		}
		
		final GitsshFactory sshSession = new GitsshFactory(null); //Yep it's not the way to go!!
		
		transport = t -> {
			final SshTransport sshTransport = (SshTransport) t;
			sshTransport.setSshSessionFactory(sshSession);
		};
		
		
	}
	
	
	//Clone repository
	public static void cloneRepo(final String repository, final String path) {
		try {
			cleanDestination(path);
			Git.cloneRepository().setDirectory(Paths.get(path).toFile()).setURI(repository).call();
		} catch (GitAPIException e) {
			e.printStackTrace();
		}
	}
	
	//List all the available branches.
	public void DisplayAllBranches() {
		try {
			List<Ref> branches = git.branchList().setListMode(ListMode.ALL).call();
			for(Ref branch : branches) {
				System.out.println("Branch name -> " + branch.getName());
			}
		} catch (GitAPIException e) {
			throw new IllegalStateException("Could not read branch list");
		}
	}
	
	// Cleaning destination folder prior cloning
	private static void cleanDestination(final String pathToClean) {
		File[] files = Paths.get(pathToClean).toFile().listFiles();
		
		if(files != null) {
			for(File file : files) {
				System.out.println(file.toString());
				try {
					FileUtils.deleteDirectory(file);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	//Commit changes to the current branch
	public final void commit(final String commitMessage) {
		try {
			git.add().addFilepattern(".").call();
			git.add().setUpdate(true).addFilepattern(".").call();
			git.commit().setAll(true).setMessage(commitMessage).setAuthor(getAuthor(userName, userEmail)).call(); 
		} catch (final GitAPIException e) {
			throw new IllegalStateException("Could not create commit ,", e);		} 
	}
	
	//Git commit and push
	public final boolean gitCommitAndPush(final String commitMessage) {
		System.out.println("Committing changes with message :" + commitMessage);
		commit(commitMessage);
		try {
			final boolean successfulPush = gitPush().setRemote("origin").call().iterator().hasNext();
			return successfulPush;
		} catch (final Exception e) {
			throw new IllegalStateException("Git push failed!" + e);
		} 
	}
	
	public PersonIdent getAuthor(final String username, final String userEmail) {
		final Config config = git.getRepository().getConfig();
		final String name = Optional.ofNullable(config.getString("user", null, "name")).orElse(username);
		final String email = Optional.ofNullable(config.getString("user", null, "email")).orElse(userEmail);
		return new PersonIdent(name, email, Date.from(LocalDate.now().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()), TimeZone.getDefault());
		
	}
	
	//Checkout to a Specific branch 
	public final void checkoutToBranch(final String toBranch) {
		System.out.println("Checking out to branch :" + toBranch);
		try {
			final Set<String> existingBranches = git.branchList()
					.call()
					.stream()
					.map(Ref::getName)
					.map(Repository::shortenRefName)
					.collect(Collectors.toSet());
			final CheckoutCommand checkoutCommand = git.checkout().setName(toBranch).setStartPoint(currentBranch());
			if(!existingBranches.contains(toBranch)) {
				checkoutCommand.setCreateBranch(true);
			}
			checkoutCommand.call();
		} catch (GitAPIException e) {
			throw new IllegalStateException(e);
		}
	}
	
	
	//Get Current branch 
	public final String currentBranch() {
		try {
			final String fullBranch = git.getRepository().getBranch();
			String branch = "";
			if(fullBranch.equals("HEAD")) {
				branch = System.getenv("BRANCH_NAME");
				if(branch == null || branch.isEmpty()) {
					throw new IllegalStateException("No branch found");
				}
			} else {
				branch = fullBranch;
			}
			System.out.println("Current branch : " + branch);
			return branch;
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}
	
	// Git push
	private final PushCommand gitPush() {
		return git.push().setTransportConfigCallback(transport);
	}
	
	//Check if repository is clean
	public final boolean isIndexClean() {
		try {
			git.add().addFilepattern(".").call();
			final Status status = git.status().call();
			return (status.isClean() && !status.hasUncommittedChanges());
		} catch (final GitAPIException e) {
			throw new IllegalStateException("Could not determing if index is clean ,", e);
		}
	}
	
	public static Optional<Path> userHome(){
		return Optional.ofNullable(System.getProperty(USER_HOME)).map(Paths::get);
	}
	
}
