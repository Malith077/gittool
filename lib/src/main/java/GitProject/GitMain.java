package GitProject;

import java.io.File;
import java.io.IOException;

public class GitMain {
	
	public static void main(String[] args) throws IOException {
		GitService.cloneRepo("git@gitlab.com:Malith077/aligent.git", "L:\\Destination\\"); 
		GitService gitService = new GitService("git@gitlab.com:Malith077/aligent.git", "L:\\Destination\\", "Malith077", "malith.git@gmail.com");
		
		gitService.DisplayAllBranches();
//		deleteFileForTest();
		gitService.checkoutToBranch("TestingGitTool-Branch-Test");
		addFile();
		gitService.gitCommitAndPush("Hope it works!!!");
		
//		System.out.println("Index Status " + gitService.isIndexClean());
	}
	
	
	//Add a file for testing
	public static void addFile() {
		try {
			File file = new File("L:\\Destination\\Testing-cache.js");
			if(file.createNewFile()) {
				System.out.println("File created " + file.toString());
			}
		} catch (Exception e) {
		
		}
	}
	
	
	//this is a test function, delete later
	public static void deleteFileForTest() {
		File toBeDel = new File("L:\\Destination\\app\\utility\\cache.js");
		try {
			if(toBeDel.delete()) {
				System.out.println("File is deletewdd " + toBeDel.getName());
			} else {
				System.out.println("Delete failed");
			}
			
		} catch (Exception e) {
			//
		}
	}
	
	
}
