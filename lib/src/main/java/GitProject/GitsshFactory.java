package GitProject;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.jgit.transport.ssh.jsch.JschConfigSessionFactory;
import org.eclipse.jgit.transport.ssh.jsch.OpenSshConfig.Host;
import org.eclipse.jgit.util.FS;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import Configurations.CredentialConfiguration;

public class GitsshFactory extends JschConfigSessionFactory{

	private static final Set<Path> SSH_KEY_PATHS = Stream
			.concat(Stream
					.of(GitService.userHome())
					.filter(Optional::isPresent)
					.map(Optional::get)
					.map(p -> p.resolve(".ssh")), Stream.of("C:\\Users\\swini\\.ssh", "Will configure this for Mac OS").map(Paths::get))
			.collect(Collectors.toSet());
	
	private static final String SSH_KEY_FILENAME = "id_rsa";
	private final Optional<CredentialConfiguration> creds;
	
	public GitsshFactory(final Optional<CredentialConfiguration> aCreds) {
		creds = aCreds; // for now we will ignore this.
	}
	
	@Override
	protected void configure(Host hc, Session session) {
		java.util.Properties config = new java.util.Properties(); 
		config.put("StrictHostKeyChecking", "no");
		session.setConfig(config);
	}

	@Override
	protected JSch createDefaultJSch(FS fs) throws JSchException {
		
		final JSch jsch = super.createDefaultJSch(fs);

//		TODO 		
//		final Optional<Path> confKeyPaths = creds.flatMap(CredentialConfiguration::sshKeyPath);
//		if(confKeyPaths.isPresent()) {
//			jsch.addIdentity(confKeyPaths.get().toString());
//			return jsch;
//		}
		
		for(final Path keyPath : SSH_KEY_PATHS) {
			final Path sshPath = keyPath.resolve(SSH_KEY_FILENAME);
			if(sshPath.toFile().exists()) {
				jsch.addIdentity(sshPath.toString());
			}
		}
		
		return jsch;
	}
	
	
}
 	