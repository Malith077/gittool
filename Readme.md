## Generating SSH-key

```
    > ssh-keygen
```
* In order to authenticate using the SSH key with Jsch, the public key needs to be formatted into ``` pem``` format.

```
    > ssh-keygen -p -f <path to>/id_rsa -m pem
```